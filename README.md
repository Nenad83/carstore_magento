# Carstore

#### This repository is Magento e-commerce aplication. 

# Prerequisites

#### To run this application you need Docker Engine 1.10.0. Docker Compose is recomended with a version 1.6.0 or later.

# How to run the aplication

#### The main folder of the repository contains a docker-compose.yml file. You can run the container with docker-compose or you can install it manually.



## Run the container with docker-compose

#### 1. Clone the repository and run the application with docker-compose
    $ docker-compose up -d
  
#### 2. Unlink etc folder from /opt/bitnami/magento/htdocs/app and copy it from  /bitnami/magento/htdocs/app/etc  
 
    $ docker exec -it magento bash
    $ unlink /opt/bitnami/magento/htdocs/app/etc
    $ cp -R /bitnami/magento/htdocs/app/etc /opt/bitnami/magento/htdocs/app/
    
#### 3. Then run magento setup upgrade

    $ /opt/bitnami/magento/htdocs/bin/magento setup:upgrade
    $ /opt/bitnami/magento/htdocs/bin/magento setup:di:compile
    
#### 4. Go to localshost/carstore/page/view    
    
    http://127.0.0.1/carstore/page/view  

## Run the container manually

#### 1. Create new network for magento and mariadb

    $ docker network create magento-tier

#### 2. Create MariaDB container

    $ docker volume create --name mariadb_data
    $ docker run -d --name mariadb \
      -e ALLOW_EMPTY_PASSWORD=yes \
      -e MARIADB_USER=bn_magento \
      -e MARIADB_PASSWORD=your_password \
      -e MARIADB_DATABASE=bitnami_magento \
      --net magento-tier \
      --volume mariadb_data:/bitnami \
      bitnami/mariadb:latest 


#### 3. Create Magento container

    $ docker volume create --name magento_data
    $ docker run -d --name magento -p 80:80 -p 443:443 \
      -e MAGENTO_DATABASE_USER=bn_magento \
      -e MAGENTO_DATABASE_PASSWORD=your_password \
      -e MAGENTO_DATABASE_NAME=bitnami_magento \
      --net magento-tier \
      --volume magento_data:/bitnami \
      bitnami/magento:latest

#### 4. Copy code folder from cloned repository to magento container

    $ tar -cvf code.tar code
    $ docker cp code.tar magento:/opt/bitnami/magento/htdocs/app
    $ docker exec -it magento bash
    $ cd /opt/bitnami/magento/htdocs/app
    $ tar -xvf code.tar
    

#### 5. Unlink etc folder from /opt/bitnami/magento/htdocs/app and copy it from  /bitnami/magento/htdocs/app/etc  
 
    $ docker exec -it magento bash
    $ unlink /opt/bitnami/magento/htdocs/app/etc
    $ cp -R /bitnami/magento/htdocs/app/etc /opt/bitnami/magento/htdocs/app/
    
#### 6. Then run magento setup upgrade

    $ /opt/bitnami/magento/htdocs/bin/magento setup:upgrade
    $ /opt/bitnami/magento/htdocs/bin/magento setup:di:compile
    
#### 7. Go to localshost/carstore/page/view    
    
    http://127.0.0.1/carstore/page/view



